<?php

use Teraception\Firebase\Messaging\Legacy\FCMClient as LegacyClient;
use Teraception\Firebase\Messaging\Legacy\Requests\AddDeviceGroupRequest;
use Teraception\Firebase\Messaging\Legacy\Requests\RemoveDeviceGroupRequest;
use Teraception\Firebase\Messaging\V1\FCMClient as V1Client;
use Teraception\Firebase\Messaging\Legacy\Requests\CreateDeviceGroupRequest;
use Teraception\Firebase\Messaging\Legacy\Requests\GetDeviceGroupRequest;
use Teraception\Firebase\Messaging\Legacy\Requests\LegacySendRequest;
use Teraception\Firebase\Messaging\V1\Requests\V1SendRequest;

require '../vendor/autoload.php';

$payload = new \Teraception\Firebase\Messaging\Legacy\SendPayload([
    'notification'=>[
        'title'=>'Title',
        'body'=>'abc'
    ]
]);

$c = new LegacySendRequest($payload);
$c->topic = 'topic';
$legacyClient = new LegacyClient('','');
$p = $legacyClient->executeAsyncOnPool($c);
$resp = $p->wait(true);
echo json_encode($resp);

$r1 = new CreateDeviceGroupRequest('adsd',['']);
$resp = $legacyClient->executeRequest($r1);
echo json_encode($resp);

$r1 = new GetDeviceGroupRequest('adsd');
$resp = $legacyClient->executeRequest($r1);
echo json_encode($resp);

$r1 = new RemoveDeviceGroupRequest('',[]);
$resp = $legacyClient->executeRequest($r1);
echo json_encode($resp);

$r1 = new AddDeviceGroupRequest('',[]);
$resp = $legacyClient->executeRequest($r1);
echo json_encode($resp);


$payload = new \Teraception\Firebase\Messaging\V1\SendPayload([
    'message'=>[
        'data' => [
            'title' => 'title',
            'body' => 'body'
        ],
        'android'=>[
            "priority" => "high"
        ],
        'apns'=>[
            'headers'=>[
                'apns-priority'=> '5'
            ],
            'payload'=>[
                'aps'=>[
                    'alert'=>[
                        'title'=>'title',
                        'body'=>'body'
                    ]
                ]
            ]
        ]
    ]
]);

$req = new V1SendRequest($payload, '');
$req->setTokens([]);
$client2 = new V1Client(realpath(__DIR__.'/auth.json'),'');
$resp = $client2->executeAsyncOnPool($req,5)->wait(true);