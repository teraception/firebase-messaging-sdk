<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:22 PM
 */

namespace Teraception\Firebase\Messaging\V1;


class FCMClient extends \Teraception\Firebase\Messaging\Base\FCMClient
{
    protected $projectId;

    /**
     * LegacyFCMClient constructor.
     * @param $projectId string Firebase project id
     * @param $authFilePath
     */
    public function __construct($authFilePath, $projectId)
    {
        parent::__construct([
            'base_uri'=>'https://fcm.googleapis.com/v1/projects/'.$projectId.'/'
        ]);
        $this->projectId = $projectId;
        $client = new \Google_Client();
        putenv("GOOGLE_APPLICATION_CREDENTIALS=".$authFilePath);
        $client->useApplicationDefaultCredentials();
        $client->addScope('https://www.googleapis.com/auth/firebase.messaging');
        $this->client = $client->authorize($this->client);
    }
}