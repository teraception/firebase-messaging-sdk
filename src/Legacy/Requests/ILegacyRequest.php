<?php
/**
 * Created by PhpStorm.
 * User: talha
 * Date: 6/11/2018
 * Time: 9:58 PM
 */

namespace Teraception\Firebase\Messaging\Legacy\Requests;


use Teraception\Firebase\Messaging\Base\Requests\IRequest;

interface ILegacyRequest extends IRequest
{

}